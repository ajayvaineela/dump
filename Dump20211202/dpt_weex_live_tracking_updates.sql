-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: dpt_weex_live
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tracking_updates`
--

DROP TABLE IF EXISTS `tracking_updates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tracking_updates` (
  `id` int NOT NULL AUTO_INCREMENT,
  `pk_id` bigint NOT NULL,
  `entry_number` int NOT NULL,
  `article_number` varchar(50) NOT NULL,
  `ean` varchar(50) NOT NULL,
  `order_id` varchar(50) NOT NULL,
  `trackingId` varchar(50) NOT NULL,
  `asn` varchar(50) NOT NULL,
  `company_id` bigint NOT NULL,
  `company_name` varchar(50) NOT NULL,
  `tracking_status` varchar(50) NOT NULL,
  `updated_0` datetime NOT NULL,
  `updated_1` datetime NOT NULL,
  `updated_2` datetime NOT NULL,
  `updated_3` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `trackingId` (`trackingId`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tracking_updates`
--

LOCK TABLES `tracking_updates` WRITE;
/*!40000 ALTER TABLE `tracking_updates` DISABLE KEYS */;
INSERT INTO `tracking_updates` VALUES (56,27,1,'1000127','1011202001','B01052790H01','09981122330101','7001000034',1000333,'aldi_north','DELIVERED','2020-11-13 05:06:48','2020-11-13 05:19:15','2020-11-13 05:20:09','2020-11-13 05:21:17'),(57,31,1,'1000126','1011202001','B01052790H01','09981122330101','7001000038',1000333,'aldi_north','DELIVERED','2020-11-13 05:06:48','2020-11-13 05:19:15','2020-11-13 05:20:09','2020-11-13 05:21:17'),(58,32,1,'ZY33140000272','1011202003','B01052790H02','09981122330101','7001000039',1000333,'aldi_north','DELIVERED','2020-11-13 05:06:48','2020-11-13 05:19:16','2020-11-13 05:20:10','2020-11-13 05:21:17'),(68,33,0,'99013420','','9844004447124269','09981122330101','7002000030',1000444,'aldi_south','DELIVERED','2020-11-13 12:25:02','0000-00-00 00:00:00','0000-00-00 00:00:00','2020-11-13 12:33:55'),(69,34,1,'1000126','1011202001','B01052790H08','','7001000040',1000333,'aldi_north','','2020-11-13 13:32:40','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `tracking_updates` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-02  5:40:14
