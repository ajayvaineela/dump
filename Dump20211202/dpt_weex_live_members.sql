-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: dpt_weex_live
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `members`
--

DROP TABLE IF EXISTS `members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `members` (
  `id` int NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `last_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `username` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `company_id` int NOT NULL,
  `company_name` varchar(50) NOT NULL,
  `trongate_user_id` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `members`
--

LOCK TABLES `members` WRITE;
/*!40000 ALTER TABLE `members` DISABLE KEYS */;
INSERT INTO `members` VALUES (1,'Carsten','Clausen','carsten','c.clausen@summary-company.com','$2y$11$ZXG1PpA6FzlAA/GlbKX7ke2tXiRv7hVhbf/jh7AXQ/IEZGM51xjMS',0,'',4),(2,'Martin','Tutlewski','admin','martin.tutlewski@intersky-consulting.de','$2y$11$G2lLiNQPWfnjcdXmwtyu2.XbyUTCsUjaVI3cB6Knl2UI6VboPKeTm',0,'',1),(3,'Balaji Sai','Vighnesh','balaji','balaji@gmail.com','$2y$11$GMZmcUdwv8GWacpeLn5Oius6HtTt.wcLEDXr6RfftmFZfe0zVHQzW',0,'',5),(4,'Martin','Tutlewski','martin','martin.tutlewski@intersky-consulting.de','$2y$11$VocZMJ8jaxMFNAgkXt6.Vuw1GC7cvDxLKP7KDIU6oeRb8RdkCgEYO',0,'',7),(6,'Parge','Felix','parge','f.parge@summary-company.com','$2y$11$mokidRBoG9/kt4t11izWletJJQ1c349/RM7g.wD3U2hnnt5dwtI12',0,'',9),(8,'it_ops','Babu','it_ops','ajay.k@intersky-consulting.de','$2y$11$xzk10reo1aFINXKBKeOAr.eYDJtFFqDVomr2IJQN373XvsbDgQm82',0,'',11),(9,'Ahmad','Tablieh','a.tablieh','a.tablieh@summary-company.com','$2y$11$07D9M7Qe4sZC9aX2SweNz.DnOxrxASbMEeFEtTvGBEmY6Y6qrI9Gm',0,'',12),(10,'Nermin','Tutlewski','nermin','nermin.tutlewski@intersky-consulting.de','$2y$11$HIo09qkFDBqVpFRphuGAuemsiWLrHOdgwpeSBtis.G6eg67k8ltam',0,'',13),(11,'Maren','Hartmann','maren.h','maren.hartmann@monolith-gmbh.com','$2y$11$pPoPNqM3EB4l8EmDGf0M7O075478ENH.B2YSapkGT.1s2vdaa6qDu',0,'',14),(12,'Marvin','Schultze','marvin.s','marvin.schulze@monolith-gmbh.com','$2y$11$NmolzslAdzv19VW31uuhheUbGf7Yz30kQYMtfD1MHrasJ0fZdBrMa',0,'',15),(13,'Mara','Haltermann','mara','M.Haltermann@summary-company.com','$2y$11$8ODxpvvTGHhEQPALYkAw7.6fRhPFuQQNCcNBCKbh3Zek1mf0HKBaC',0,'',16),(14,'Bhautik','Bhautik','bhautik','ahmad.summary57@gmail.com','$2y$11$sP9zbO57l55DAi9hu3Re4eeQakkCmSVwFGmEgVTFHCSjZpmqLoSpi',0,'',17),(16,'Jana','Laackmann','j.laackmann','j.laackmann@servicecenter.info','$2y$11$BA0WM8f9JgEW9ziPf3F2M.ghQfhrpxMOXl6onQsnjrK.FPkqnyphG',0,'',19),(19,'Larissa','Grabbe-Hauschild','l.grabbe-hauschild','l.grabbe-hauschild@servicecenter.info','$2y$11$rJNbwjul1.OczgQ5nuX0LetwaRg2wBU2wp/4FAnoz8SOi7tRa9Geu',0,'',22),(21,'Lena','Dreifke','l.dreifke','l.dreifke@servicecenter.info','$2y$11$xQbDCOpt9xR76IDqBpuz8eEQNdKlXSqnFGDmd7Zt3FyNLlXaywjk.',0,'',24),(22,'Birte','Wollank','b.wollank','b.wollank@servicecenter.info','$2y$11$9suOvcSc3NGsRrfxj5Pk4OTH4bUyXb2dl8tQahMB06kh4rGPePPqi',0,'',25),(23,'Niklas','Holzknecht','n.holzknecht','N.Holzknecht@summary-company.com','$2y$11$Ly9wcFCIQdna9cVZ2N0rKei7WL/OfvyeDDutdWtVSSSMKR0vGZZQ.',0,'',26),(24,'Heiko','Hagenstein','h.hagenstein','H.Hagenstein@summary-company.com','$2y$11$8mUnaewtCNd9wx.qNUZu5eLoMCOnaH2RWDHtL.r952Jhytiv4qTfq',0,'',27),(26,'Jasmin','Mohr','j.mohr','j.mohr@servicecenter.info','$2y$11$t6zGlhX7b8o60eVZuZf47eyWPgSa.z7UrSYIKxWpZ2Sxv6wtblKn6',0,'',29),(27,'Britta','Stahn','b.stahn','b.stahn@servicecenter.info','$2y$11$obTDPi9fj83lHRmA.DzQD.Vvp6QpLBLZy8JeWuHoWNYXn/rWsZR/q',0,'',30),(28,'Natascha','Geese','n.geese','n.geese@servicecenter.info','$2y$11$SahqsROCkMPLFCND7giJSuagxZ8kZDgwBl7MAhEUIfwAG0Iz46cRm',0,'',31),(30,'Matthias','Sauthof','m.sauthof','M.Sauthof@summary-company.com','$2y$11$SYDeM.pGIrKyKJqkL.NutuAlD.dTj.dC.SFJSjkga70Qwr1FdU3DS',0,'',33),(31,'Thorben','Paulat','t.paulat','t.Paulat@summary-company.com','$2y$11$0JuQG0yflFbxNHH6YE13VOVyyTMhsvdynLrPArMR1z6Vlh2/V.8M.',0,'',34),(33,'Lennart','Neumann','lennart.neumann','lennart.neumann@zoomyo.com','$2y$11$KkL5QP/fMJ1IDZj6hfKjBe9kF74nKss1jSIlD8vV9f6d0sSSHmmWi$2y$11$R6Nz3EC4QuAZo.k.swiGcej7K0EceHyFNL.W.Tkyc/LDJuRXzOVaG',0,'',36),(34,'Lennart','Neumann','lennart.neumann1','lennart.neumann@zoomyo.com','$2y$11$KUFW6hnGV7Rorij5K6toi.5EJoRuPnTrDFk/lCyPaRws4SPof32fK',0,'',37),(35,'Lennart','Neumann','lennartn','l.neumann@servicecenter.info','$2y$11$.HMbW4uRDvzDeXEUxmwhAOPI59o7TQ8Wia14PldhLeNJa4D7FUi6K',0,'',38);
/*!40000 ALTER TABLE `members` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-02  5:41:35
