-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: dpt_weex_live
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `trongate_administrators`
--

DROP TABLE IF EXISTS `trongate_administrators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `trongate_administrators` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(65) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `password` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `trongate_user_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trongate_administrators`
--

LOCK TABLES `trongate_administrators` WRITE;
/*!40000 ALTER TABLE `trongate_administrators` DISABLE KEYS */;
INSERT INTO `trongate_administrators` VALUES (1,'admin','$2y$11$SoHZDvbfLSRHAi3WiKIBiu.tAoi/GCBBO4HRxVX1I3qQkq3wCWfXi',1),(2,'Second admin user','$2y$11$AIsbH14X/jHY8se8F/V1VuwM/75XNpEXJFyrjg2wrb1QI3tLuX7CG',2),(3,'balaji','$2y$11$GMZmcUdwv8GWacpeLn5Oius6HtTt.wcLEDXr6RfftmFZfe0zVHQzW',5),(4,'carsten','$2y$11$ZXG1PpA6FzlAA/GlbKX7ke2tXiRv7hVhbf/jh7AXQ/IEZGM51xjMS',6),(5,'martin','$2y$11$VocZMJ8jaxMFNAgkXt6.Vuw1GC7cvDxLKP7KDIU6oeRb8RdkCgEYO',7),(6,'sebastian','$2y$11$GyCR7cAMZwDFqxJrDA1kk.iQOJVYi0qa.VK7Sfv/cGeZ2qORemFF2',8),(7,'parge','$2y$11$g2nDBoNNEtLtnFWop8T7eONzdwRg3SzyTbRALa.N4Z9CejSFyX8OK',9),(8,'daniela','$2y$11$kUyHyELtDtnw.jLH3STU1uMfx34poirr.n6291q.13SnwDXcK2Mey',10),(9,'ajay','$2y$11$xzk10reo1aFINXKBKeOAr.eYDJtFFqDVomr2IJQN373XvsbDgQm82',11);
/*!40000 ALTER TABLE `trongate_administrators` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-02  5:40:51
