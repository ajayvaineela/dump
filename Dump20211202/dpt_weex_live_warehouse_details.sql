-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: dpt_weex_live
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `warehouse_details`
--

DROP TABLE IF EXISTS `warehouse_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `warehouse_details` (
  `id` int NOT NULL AUTO_INCREMENT,
  `carrier_reference` varchar(100) NOT NULL,
  `source_warehouse_name` varchar(255) NOT NULL,
  `source_company` varchar(100) NOT NULL,
  `source_country` varchar(100) NOT NULL,
  `source_city` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `source_street` varchar(50) NOT NULL,
  `source_zip_code` varchar(50) NOT NULL,
  `asn_starts_with` tinyint NOT NULL,
  `csv_prefix` varchar(10) NOT NULL,
  `outbound_ip` varchar(50) NOT NULL,
  `outbound_username` varchar(50) NOT NULL,
  `outbound_password` varchar(50) NOT NULL,
  `inbound_ip` varchar(50) NOT NULL,
  `inbound_username` varchar(50) NOT NULL,
  `inbound_password` varchar(50) NOT NULL,
  `user_id` int NOT NULL,
  `status` tinyint NOT NULL DEFAULT '1',
  `protocol_type` varchar(10) NOT NULL,
  `port_number` varchar(10) NOT NULL,
  `access_outbound_folder` varchar(255) NOT NULL,
  `access_inbound_folder` varchar(255) NOT NULL,
  `local_path` varchar(100) NOT NULL,
  `folder_location` text NOT NULL,
  `warehouse_code` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `sftp_status` tinyint NOT NULL,
  `sftp_whv_generation` tinyint NOT NULL,
  `sftp_feedback` varchar(5) NOT NULL,
  `feedback_trans` int NOT NULL,
  `pod_trans` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `warehouse_details`
--

LOCK TABLES `warehouse_details` WRITE;
/*!40000 ALTER TABLE `warehouse_details` DISABLE KEYS */;
INSERT INTO `warehouse_details` VALUES (12,'','17111','17111 Logistik GmbH','DE','Osterrönfeld','August-Borsig-Str. 11','24783',3,'SUM','173.249.60.179','17111','WNK2Qj6!#+5aPb_9','173.249.60.179','summary','#Xy_#m6Lm36+d3Jf',1,0,'ftps','','','','','','17111_old',0,0,'',0,0),(14,'','17111','17111 Logistik GmbH','DE','Osterrönfeld','August-Borsig-Str. 11','24783',7,'SUM','173.249.60.179','deeptrace-out-17111',':oJi*LQxB\'e2HKba','173.249.60.179','deeptrace-in-17111','Dp6.KMkuNPolHjP?',1,1,'ftps','','','','','','17111_ftps',0,0,'',0,0),(15,'','CLC China Logistic Center GmbH','CLC Halle P + CLC Bereitstellung','DE','Itzehoe','Vossbarg 1','25524',7,'SUM','173.249.60.179','deeptrace-out-clc','.2;Ljpq3e3^#?H/','173.249.60.179','deeptrace-in-clc','cWAmSgFb^92P#,tO',1,1,'ftps','','','','','','clc',0,0,'',0,0),(18,'','Amm GmbH & Co KG Spedition','Amm GmbH & Co KG Spedition','DE','Nürnberg','Hamburger Str. 99','90451',0,'SUM','173.249.60.179','deeptrace-out-backup','.NTy@`qKFu/q/.xj','173.249.60.179','deeptrace-in-backup1','th,/Jq?:2H\"#\"cJP',1,1,'ftps','','','','','','amm',0,0,'',0,0),(20,'','','','','','','',0,'','mft-int.xais.de','sys-dt','_3&bRasI3&4?8EH8rEc&','mft-int.xais.de','sys-dt','_3&bRasI3&4?8EH8rEc&',1,1,'sftp','22','','','','','sys-dt',0,0,'',0,0),(21,'','CLC China Logistic Center GmbH','CLC Halle P + CLC Bereitstellung','DE','Itzehoe','Vossbarg 1','25524',0,'SUM','mft-int.xais.de','sys-dt','_3&bRasI3&4?8EH8rEc&','mft-int.xais.de','sys-dt','_3&bRasI3&4?8EH8rEc&',1,1,'sftp','22','/dt-clc/pub/outbound/','/dt-sbb/pub/inbound/','public/to_warehouse/sbb','','sbb',1,0,'',0,0),(22,'','','','','','','',0,'SUM','mft-int.xais.de','sys-dt','_3&bRasI3&4?8EH8rEc&','mft-int.xais.de','sys-dt','_3&bRasI3&4?8EH8rEc&',1,1,'sftp','22','/dt-aldi/pub/outbound/','/dt-aldi/pub/inbound/','public/to_warehouse/aldi_hofer_edi','','aldi_hofer_edi',1,0,'',0,0),(23,'','','','','','','',0,'','mft-int.xais.de','sys-dt','_3&bRasI3&4?8EH8rEc&','mft-int.xais.de','sys-dt','_3&bRasI3&4?8EH8rEc&',1,1,'sftp','22','/dt-plab/pub/outbound/','/dt-plab/pub/inbound/','public/to_warehouse/parcel_lab','','plab',1,0,'',0,0),(24,'','17111','17111 Logistik GmbH','DE','Osterrönfeld','August-Borsig-Str. 11','24783',7,'SUM','mft-int.xais.de','sys-dt','_3&bRasI3&4?8EH8rEc&','mft-int.xais.de','sys-dt','_3&bRasI3&4?8EH8rEc&',1,1,'sftp','22','/dt-17111/pub/outbound/','/dt-17111/pub/inbound/','public/to_warehouse/17111','','17111',1,1,'1',0,0),(25,'','TRANSDANUBIA Speditionsges.m.b.H.','TRANSDANUBIA Speditionsges.m.b.H.','AT','Pasching','Pluskaufstrasse 11','A-4061',7,'SUM','mft-int.xais.de','sys-dt','_3&bRasI3&4?8EH8rEc&','mft-int.xais.de','sys-dt','_3&bRasI3&4?8EH8rEc&',1,1,'sftp','22','/dt-au1/pub/outbound/','/dt-au1/pub/inbound/','public/to_warehouse/trans','','trans',1,1,'0',1,0),(26,'','TRANSDANUBIA Speditionsges.m.b.H.','TRANSDANUBIA Speditionsges.m.b.H.','AT','Pasching','Pluskaufstrasse 11','A-4061',0,'SUM','mft-int.xais.de','sys-dt','_3&bRasI3&4?8EH8rEc&','mft-int.xais.de','sys-dt','_3&bRasI3&4?8EH8rEc&',1,1,'sftp','22','/dt-au1/pub/outbound/','/dt-au1/pub/inbound/','public/whv_feedback_trans/pod/','','pod_trans',0,0,'0',0,1);
/*!40000 ALTER TABLE `warehouse_details` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-02  5:40:57
