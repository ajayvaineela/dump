-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: dpt_weex_live
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `raw_data`
--

DROP TABLE IF EXISTS `raw_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `raw_data` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `EINGANGSDATUM` datetime NOT NULL,
  `ASN` bigint NOT NULL,
  `ALDI_AUFTRAG` varchar(100) NOT NULL,
  `ADR_NAME` varchar(100) NOT NULL,
  `ADR_STRASSE` varchar(100) NOT NULL,
  `streetNumber` varchar(10) NOT NULL,
  `ADR_PLZ` varchar(100) NOT NULL,
  `ADR_ORT` varchar(100) NOT NULL,
  `ADR_LAND` varchar(100) NOT NULL,
  `ADR_EMAIL` varchar(100) NOT NULL,
  `TRACKING_ID` varchar(20) NOT NULL,
  `PRODUCT_NAME` varchar(100) NOT NULL,
  `ARTIKELNR` varchar(100) NOT NULL,
  `InternalArticleNumber` varchar(100) NOT NULL,
  `Summary_Article_Number` varchar(100) NOT NULL,
  `VERSANDKANAL` varchar(100) NOT NULL,
  `Abgerechnet` varchar(100) NOT NULL,
  `Retoure` varchar(100) NOT NULL,
  `ReturnDate` datetime NOT NULL,
  `campaign_id` varchar(50) NOT NULL,
  `company_name` varchar(50) NOT NULL,
  `return_tracking_id` varchar(50) NOT NULL,
  `BESTELLSTATUS` varchar(50) NOT NULL,
  `filter_by` varchar(100) NOT NULL,
  `user_id` int NOT NULL,
  `warehouse` varchar(50) NOT NULL,
  `carrier` varchar(40) NOT NULL,
  `return_carrier_name` varchar(40) NOT NULL,
  `whv_sent_on` datetime NOT NULL,
  `whv_received_on` datetime NOT NULL,
  `feedback_status` varchar(50) NOT NULL,
  `statusText` varchar(255) NOT NULL,
  `current_parcel_status` varchar(255) NOT NULL,
  `return_processing_status` varchar(50) NOT NULL,
  `return_closed_date` datetime NOT NULL,
  `inserted_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `raw_data`
--

LOCK TABLES `raw_data` WRITE;
/*!40000 ALTER TABLE `raw_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `raw_data` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-02  5:40:55
