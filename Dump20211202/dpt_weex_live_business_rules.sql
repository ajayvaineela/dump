-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: dpt_weex_live
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `business_rules`
--

DROP TABLE IF EXISTS `business_rules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `business_rules` (
  `id` int NOT NULL,
  `sequence` int NOT NULL,
  `data_type` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `source` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `target` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `client_id` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `description` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `active_status` tinyint NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `business_rules`
--

LOCK TABLES `business_rules` WRITE;
/*!40000 ALTER TABLE `business_rules` DISABLE KEYS */;
INSERT INTO `business_rules` VALUES (5,1,'order','PRID-10-1-20-20','PRID-10-1-40-20','1001','Requesting third party API',1),(6,2,'Order','PRID-10-1-40-20','PRID-10-1-20-40','1001','Write Response to DB',1),(7,3,'delivery','PRID-10-1-20-40','PRID-10-1-100-20','1001','Sending Request to warehouse',1),(8,4,'delivery','PRID-10-1-20-100','PRID-10-1-20-120','1001','Getting Response from warehouse',1),(9,5,'delivery','PRID-10-1-20-120','PRID-10-1-20-180','1001','Sending Delivery info to ALDI',1),(10,6,'delivery','PRID-10-1-20-180','PRID-10-1-20-200','1001','Getting Delivery confirmation from ALDI',1),(11,7,'delivery','PRID-10-1-20-200','PRID-10-1-20-220','1001','Asking Carrier Registration for delivery',1),(12,8,'delivery','PRID-10-1-20-220','PRID-10-1-20-240','1001','Getting registration response from the carrier',1),(13,9,'delivery','PRID-10-1-20-240','PRID-10-1-20-260','1001','Updating Tracking Id sending to ALDI',1),(14,10,'delivery','PRID-10-1-20-260','PRID-10-1-20-280','1001','Tracking Id confirmation from ALDI',1);
/*!40000 ALTER TABLE `business_rules` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-02  5:41:11
