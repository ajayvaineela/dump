-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: dpt_weex_live
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dt_countries`
--

DROP TABLE IF EXISTS `dt_countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dt_countries` (
  `id` int NOT NULL,
  `country_code` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `country_name` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status` int NOT NULL,
  `created_on` datetime NOT NULL,
  `created_by` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dt_countries`
--

LOCK TABLES `dt_countries` WRITE;
/*!40000 ALTER TABLE `dt_countries` DISABLE KEYS */;
INSERT INTO `dt_countries` VALUES (1,'+32','Belgien',1,'2020-06-02 20:42:28',1),(2,'+33','Frankreich',1,'2020-06-02 20:42:28',1),(3,'+351','Portugal',1,'2020-06-02 20:42:28',1),(4,'+352','Luxemburg',1,'2020-06-02 20:42:28',1),(5,'+39','Italien',1,'2020-06-02 20:42:28',1),(6,'+41','Schweiz',1,'2020-06-02 20:42:28',1),(7,'+43','Österreich',1,'2020-06-02 20:42:28',1),(8,'+44','Vereinigtes Königreich',1,'2020-06-02 20:42:28',1),(9,'+45','Dänemark',1,'2020-06-02 20:42:28',1),(10,'+46','Schweden',1,'2020-06-02 20:42:28',1),(11,'+49','Deutschland',1,'2020-06-02 20:42:28',1),(12,'+48','Polen',1,'2020-06-02 20:42:28',1);
/*!40000 ALTER TABLE `dt_countries` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-02  5:40:56
