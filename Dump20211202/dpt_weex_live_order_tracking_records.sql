-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: dpt_weex_live
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `order_tracking_records`
--

DROP TABLE IF EXISTS `order_tracking_records`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_tracking_records` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `file_type` varchar(256) DEFAULT NULL,
  `file_path` varchar(255) DEFAULT NULL,
  `is_processed` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `process` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_tracking_records`
--

LOCK TABLES `order_tracking_records` WRITE;
/*!40000 ALTER TABLE `order_tracking_records` DISABLE KEYS */;
INSERT INTO `order_tracking_records` VALUES (1,'2021-11-29 07:02:40','text/csv','public\\trackingCsv\\075aafe8ec9d6a7e70b9b43919905257',1,1,'REPLACE_TRACKING_ID','2021-11-29 07:02:40'),(2,'2021-11-29 09:15:55','text/csv','public\\trackingCsv\\90d91283dcc7697ccbe92d3d27084bd2',1,1,'REPLACE_TRACKING_ID','2021-11-29 09:15:55'),(3,'2021-11-29 09:16:31','text/csv','public\\trackingCsv\\03e549b38168347c930ae7880b4583b4',1,1,'REPLACE_TRACKING_ID','2021-11-29 09:16:31'),(4,'2021-11-29 09:21:58','text/csv','public\\trackingCsv\\758959366cde053b13b6c0a1741d8361',1,1,'REPLACE_TRACKING_ID','2021-11-29 09:21:58'),(5,'2021-11-29 09:22:30','text/csv','public\\trackingCsv\\4b14722bd6ac6d09f1ec7a496e28032d',1,1,'REPLACE_TRACKING_ID','2021-11-29 09:22:30'),(6,'2021-11-29 09:26:54','text/csv','public\\trackingCsv\\89b35657f4f6bfa4ab32eeb376a8c1d1',1,1,'REPLACE_TRACKING_ID','2021-11-29 09:26:54'),(7,'2021-11-29 09:27:01','text/csv','public\\trackingCsv\\a08ed398ff38aff90528009469467af5',1,1,'REPLACE_TRACKING_ID','2021-11-29 09:27:01'),(8,'2021-11-29 09:27:03','text/csv','public\\trackingCsv\\80ff817feebe94e88c7b31e071a1b8b0',1,1,'REPLACE_TRACKING_ID','2021-11-29 09:27:03'),(9,'2021-11-30 05:50:41','text/csv','public\\trackingCsv\\0aff30cfd98db45346cc5397f8582cf8',1,1,'REPLACE_TRACKING_ID','2021-11-30 05:50:41'),(10,'2021-11-30 05:51:43','text/csv','public\\trackingCsv\\eafebcf23db93d77cb383a87be4a24ae',1,1,'REPLACE_TRACKING_ID','2021-11-30 05:51:43'),(11,'2021-11-30 05:51:46','text/csv','public\\trackingCsv\\812b29e8222054559d68910c548c8dcf',1,1,'REPLACE_TRACKING_ID','2021-11-30 05:51:46'),(12,'2021-11-30 06:02:48','text/csv','public\\trackingCsv\\d5043319b9aa26ec43cb5f47ab3f9aed',1,1,'REPLACE_TRACKING_ID','2021-11-30 06:02:48'),(13,'2021-11-30 06:19:24','text/csv','public\\trackingCsv\\7344766f2ef4014c53422b289b0f6da7',1,2,'ADD_TRACKING_ID','2021-11-30 06:19:24'),(14,'2021-11-30 06:19:53','text/csv','public\\trackingCsv\\f8ad6383a38de814ab5bf6f0c95d0e78',1,2,'ADD_TRACKING_ID','2021-11-30 06:19:53'),(15,'2021-11-30 11:58:56','text/csv','public\\trackingCsv\\b6c59f475f0a06d09d587161d09fc1f4',1,14,'REPLACE_TRACKING_ID','2021-11-30 11:58:56');
/*!40000 ALTER TABLE `order_tracking_records` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-02  5:40:30
