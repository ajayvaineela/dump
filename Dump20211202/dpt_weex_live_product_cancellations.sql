-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: dpt_weex_live
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `product_cancellations`
--

DROP TABLE IF EXISTS `product_cancellations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_cancellations` (
  `id` int NOT NULL AUTO_INCREMENT,
  `order_id` varchar(50) NOT NULL,
  `entry_number` int NOT NULL,
  `cancelled_quantity` int NOT NULL,
  `cancel_reason` varchar(255) NOT NULL,
  `notes` varchar(255) NOT NULL,
  `cancelled_by` int NOT NULL,
  `cancelled_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_cancellations`
--

LOCK TABLES `product_cancellations` WRITE;
/*!40000 ALTER TABLE `product_cancellations` DISABLE KEYS */;
INSERT INTO `product_cancellations` VALUES (17,'B01074292H05',1,1,'CUSTOMER_REQUEST','As Per customer request',1,'2020-11-16 14:13:16'),(18,'B01107097H26',1,1,'CUSTOMER_REQUEST','Kunde hat versehentlich eine doppelte Bestellung ausgelöst',6,'2021-01-07 13:05:44'),(19,'B01108493H87',1,1,'CUSTOMER_REQUEST','Gewichtsmässig ist Kunde zu stark - bittet um Storno',6,'2021-01-07 14:23:39'),(20,'B01106698H15',1,1,'CUSTOMER_REQUEST','Storno auf Kundenwunsch',6,'2021-01-08 13:02:07'),(21,'B01108191H12',1,1,'CUSTOMER_REQUEST','Hat ein Curved bestellt',6,'2021-01-08 14:13:43'),(22,'B01110095H40',1,1,'CUSTOMER_REQUEST','Versehentliche Doippelbestellung',6,'2021-01-11 09:02:25'),(23,'B01106798H43',1,1,'CUSTOMER_REQUEST','Kundenstorno',5,'2021-01-11 10:12:49'),(24,'B01106798H43',2,1,'CUSTOMER_REQUEST','Kundenstorno',5,'2021-01-11 10:12:50'),(25,'B01112791H70',1,1,'CUSTOMER_REQUEST','Möchte das Starter bestellen',6,'2021-01-18 09:38:12'),(26,'B01124590H04',1,1,'CUSTOMER_REQUEST','Stornierung - Mail von Carsten Clausen am 03.02.2021',6,'2021-02-03 12:35:06'),(27,'B01100394H80',1,1,'CUSTOMER_REQUEST','Stornierung auf Kundenwunsch',6,'2021-02-03 16:56:25'),(28,'B01127095H49',1,1,'CUSTOMER_REQUEST','Kd. hat Wunsch eines Stornos',6,'2021-02-08 13:26:12'),(29,'B01116591H27',1,1,'CUSTOMER_REQUEST','Storno vom Kunden ',6,'2021-02-12 13:39:54'),(30,'B01115790H87',1,1,'CUSTOMER_REQUEST','Kunde hat Bestellung storniert',6,'2021-02-17 11:47:37'),(31,'B01131790H37',1,1,'CUSTOMER_REQUEST','Power Rack ',5,'2021-02-20 09:07:27'),(32,'B01131690H47',1,1,'CUSTOMER_REQUEST','Kunde wünscht Stornierung. ',5,'2021-02-22 11:19:50'),(33,'B01132591H42',1,1,'CUSTOMER_REQUEST','Kundin möchte Bestellung stornieren',6,'2021-02-22 13:56:30'),(34,'B01149096H93',1,1,'CUSTOMER_REQUEST','Nach Stornierungsbestätigung im Lager auf Kundenwunsch eingetragen',6,'2021-03-17 13:17:16'),(35,'B01149396H38',1,1,'CUSTOMER_REQUEST','Kunde hat Bestellung storniert ',6,'2021-03-17 17:02:46'),(36,'B01149595H56',1,1,'CUSTOMER_REQUEST','Kunde hat Bestellung storniert',6,'2021-03-17 17:15:55'),(37,'B01149496H32',1,1,'CUSTOMER_REQUEST','Kunde hat Bestellung storniert',6,'2021-03-17 17:19:27'),(38,'B01149595H89',1,1,'CUSTOMER_REQUEST','Kunde hat Bestellung storniert',6,'2021-03-17 17:21:51'),(39,'B01149595H54',1,1,'CUSTOMER_REQUEST','Kunde hat Bestellung storniert ',6,'2021-03-17 17:25:10'),(40,'B01149595H54',2,1,'CUSTOMER_REQUEST','Kunde hat Bestellung storniert ',6,'2021-03-17 17:25:11'),(41,'B01149096H96',1,1,'CUSTOMER_REQUEST','Kunde hat Bestellung storniert.',6,'2021-03-18 10:14:10'),(42,'B01152593H78',1,1,'CUSTOMER_REQUEST','auf Kundenwunsch Storno vor Versand',6,'2021-03-24 13:03:05'),(43,'B01164196H37',5,1,'CUSTOMER_REQUEST','Nach Rücksprache Al',6,'2021-04-07 15:58:53'),(44,'B01164297H37',1,1,'CUSTOMER_REQUEST','Storno',6,'2021-04-08 17:55:43'),(45,'B01161198H40',1,1,'CUSTOMER_REQUEST','Käufer Storno',6,'2021-04-12 13:20:13'),(46,'B01181290H02',1,1,'CUSTOMER_REQUEST','Storno',6,'2021-04-28 15:48:49'),(47,'B01181190H01',1,1,'CUSTOMER_REQUEST','Kunde hat Bestellung storniert laut CC',6,'2021-04-29 10:17:47'),(48,'B01179891H14',1,1,'CUSTOMER_REQUEST','Kunde hat Bestellung storniert',6,'2021-05-05 11:27:32'),(49,'B01180090H12',1,1,'CUSTOMER_REQUEST','Kunde möchte Bestellung stornieren.',6,'2021-05-06 12:23:37'),(50,'B01179690H79',1,1,'CUSTOMER_REQUEST','Storno',6,'2021-05-12 17:22:33'),(51,'B01180290H89',1,1,'CUSTOMER_REQUEST','Kunden Storno',6,'2021-05-17 15:37:46'),(52,'B01180290H89',2,1,'CUSTOMER_REQUEST','Kunden Storno',6,'2021-05-17 15:37:47'),(53,'B01216192H27',1,1,'CUSTOMER_REQUEST','Kunde möchte Bestellung stornieren. ',6,'2021-05-31 12:35:17'),(54,'B01216192H27',2,1,'CUSTOMER_REQUEST','Kunde möchte Bestellung stornieren. ',6,'2021-05-31 12:35:18'),(55,'B01257994H04',1,1,'CUSTOMER_REQUEST','Strono',9,'2021-09-07 16:21:33'),(56,'B01028596P16',1,1,'CUSTOMER_REQUEST','SS',9,'2021-10-15 08:24:45'),(57,'B01028596P16',2,1,'CUSTOMER_REQUEST','SS',9,'2021-10-15 08:24:47'),(58,'B01047590P64',1,1,'CUSTOMER_REQUEST','Kunde möchte stornieren',6,'2021-10-26 10:24:54'),(59,'B01051896P66',1,1,'CUSTOMER_REQUEST','Falsche Farbe bestellt',6,'2021-10-28 14:40:36'),(60,'B01054092P48',2,1,'CUSTOMER_REQUEST','Kundenstorno ',6,'2021-11-01 08:24:27'),(61,'B01058491P20',1,1,'CUSTOMER_REQUEST','Kundenstorno',6,'2021-11-03 14:59:27'),(62,'B01067294P33',1,1,'CUSTOMER_REQUEST','Kundenstorno ',6,'2021-11-15 10:29:17'),(63,'B01067496P28',1,1,'CUSTOMER_REQUEST','Kundenstorno',6,'2021-11-16 08:46:09'),(64,'B01062396P27',1,1,'CUSTOMER_REQUEST','Info von Aldi',9,'2021-11-16 10:53:55'),(65,'B01061296P06',1,1,'CUSTOMER_REQUEST','Kundenstorno',6,'2021-11-16 10:57:57'),(66,'B01068191P38',1,1,'CUSTOMER_REQUEST','Doppelbestellung ',6,'2021-11-17 09:03:17'),(67,'B01070398P19',1,1,'CUSTOMER_REQUEST','Kundenstorno',6,'2021-11-18 11:23:12'),(68,'B01072893P30',1,1,'CUSTOMER_REQUEST','Kundenstorno',6,'2021-11-22 13:59:54');
/*!40000 ALTER TABLE `product_cancellations` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-02  5:41:11
