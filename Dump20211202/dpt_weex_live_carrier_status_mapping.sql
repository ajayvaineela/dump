-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: dpt_weex_live
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `carrier_status_mapping`
--

DROP TABLE IF EXISTS `carrier_status_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `carrier_status_mapping` (
  `id` int NOT NULL AUTO_INCREMENT,
  `company_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `client_status` varchar(50) NOT NULL,
  `internal_status` varchar(50) NOT NULL,
  `carrier_name` varchar(50) NOT NULL,
  `carrier_status` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carrier_status_mapping`
--

LOCK TABLES `carrier_status_mapping` WRITE;
/*!40000 ALTER TABLE `carrier_status_mapping` DISABLE KEYS */;
INSERT INTO `carrier_status_mapping` VALUES (1,'aldi_north','','ACCEPTED','DPD','ACCEPTED'),(2,'aldi_north','READY','READY_FOR_PICKUP','DPD','AT_SENDING_DEPOT'),(3,'aldi_north','','ON_THE_ROAD','DPD','ON_THE_ROAD'),(4,'aldi_north','SHIPPED','SHIPPED','DPD','AT_DELIVERY_DEPOT'),(5,'aldi_north','DELIVERED','DELIVERED','DPD','DELIVERED'),(6,'aldi_south','','ACCEPTED','DPD','ACCEPTED'),(7,'aldi_south','','READY_FOR_PICKUP','DPD','AT_SENDING_DEPOT'),(8,'aldi_south','','ON_THE_ROAD','DPD','ON_THE_ROAD'),(9,'aldi_south','','AT_DELIVERY_DEPOT','DPD','AT_DELIVERY_DEPOT'),(10,'aldi_south','DELIVERED','DELIVERED','DPD','DELIVERED');
/*!40000 ALTER TABLE `carrier_status_mapping` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-02  5:40:45
