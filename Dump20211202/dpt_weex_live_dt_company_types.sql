-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: dpt_weex_live
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dt_company_types`
--

DROP TABLE IF EXISTS `dt_company_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dt_company_types` (
  `id` int NOT NULL,
  `company_type` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `english_name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status` tinyint NOT NULL,
  `created_on` datetime NOT NULL,
  `created_by` tinyint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dt_company_types`
--

LOCK TABLES `dt_company_types` WRITE;
/*!40000 ALTER TABLE `dt_company_types` DISABLE KEYS */;
INSERT INTO `dt_company_types` VALUES (1,'B2B Kunde','B2B Client',1,'2020-06-01 16:09:37',1),(2,'Lager','Warehouse',1,'2020-06-01 16:09:37',1),(3,'Logistiker','Logistic Company',1,'2020-06-01 16:09:37',1),(4,'Carrier','Carrier parcel transportation',1,'2020-06-01 16:09:37',1),(5,'Spedition','Carrier pallet transportation',1,'2020-06-01 16:09:37',1),(6,'Reederei ','shipping company',1,'2020-06-01 16:09:37',1),(7,'Hersteller','Manufacturer',1,'2020-06-01 16:09:37',1),(8,'Großhändler','Whole Saler',1,'2020-06-01 16:09:37',1),(9,'Fulfilment Partner ','Fulfilment Partner',1,'2020-06-01 16:09:37',1),(11,'Sonstige','',1,'2020-06-10 07:36:42',1);
/*!40000 ALTER TABLE `dt_company_types` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-02  5:40:47
