-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: dpt_weex_live
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `trongate_users`
--

DROP TABLE IF EXISTS `trongate_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `trongate_users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `user_level_id` int DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trongate_users`
--

LOCK TABLES `trongate_users` WRITE;
/*!40000 ALTER TABLE `trongate_users` DISABLE KEYS */;
INSERT INTO `trongate_users` VALUES (1,'t0X0cZ0e5cuQt9xZ851b5WbieyjeHUpk',1),(2,'Uae5WmEwYjmqEmgCndQsmDh3rwvN5rE4',1),(4,'T6RaDn5DWU57FZs86rNzADxPJMjtFrSW',2),(5,'FSPKMAvg3Q6DvXeTZKC3FkJhK7bt9bNL',1),(6,'vybHwYU8QMf7WZ5Naub2BDwbZFsXqBua',1),(7,'NNVn6WZ8cEcbbB9XmMtnC6f3rAKrjVgc',1),(8,'vg6eBdPua9sKPTPebKzajfV9B7zhHLbR',1),(9,'57V6TfZJmfLde4bUZckNjU2BNFWzaxmm',1),(10,'6MZUJamFSLHHRgXDZwP8yU9BS9ar8M35',1),(11,'pzCYYWkhvg596YSapGVfE8cnkpUb6kx6',1),(12,'YGyP5eRkcH8tYHUkTxHxKMzgShnEpzs2',2),(13,'RzhBE43PMzqTucU7gdDYgCZBBqnwEwfj',2),(14,'8USmjtzgeZFmnd8qBaTTw9S7akhz2mVD',2),(15,'ETrJuCqgtKBdb4Uc4v9vnkFu6thYhNBG',2),(16,'ek84K6TqZbZZFKHUtHNvqGDJBbUF9FE2',2),(17,'Rx2cLE7r8dF8aP9sSYXJCdEYMAjxd3LV',2),(18,'Fjj4QkaP6Suf7Xdfppr2h5b5AcQMs5Wa',2),(19,'YyczU3fu5Q2mpjvuvCa6gTMsEJHhBXDW',2),(20,'S9AZ8UrWpDrE9Mu9mhXRzA5j5pSPjvyu',2),(21,'69UPgR643qUufMRunHUEr4wjfEvUCBBY',2),(22,'JFyezcdQhwpeNVPwfYwU2RdQVubN5ese',2),(23,'sZaVz6RLPm4tFx4s7ZxF6xeDF4ueU2d3',2),(24,'Db3RAaFTtGNv9ERyw43YGhCwfJFJQQU9',2),(25,'Kzy2JRVvWZtmXPaxeGzsUSYwwtcQBXhU',2),(26,'ZrF8AAWUbUcND4fYzpBAccdEf77Q5q93',2),(27,'QyrmgzjjJEVPptsAywguR72cVpGepkv4',2),(28,'2rPFeQffRaEQeTxctHhVaKqutx5Dvxse',2),(29,'Nsjwwvu7TKgqnZk7Ed4Kj7trFN2U9SFr',2),(30,'byFw9SDhRFs9czYEDdTpeNH7arFZpxky',2),(31,'UMbnQjUu6ZZ6hTwF6h9kybVk9WggADbB',2),(32,'wTdnwj7XXZSzmTzRydcnsr4Vavx7nHL5',2),(33,'pcvymj9uU8E2MWu3fmxpHF7uEqzNgaYn',2),(34,'HPv73GuGTyEvuUfaajjAkfxFuTYJCwgs',2),(35,'QBpSZPs6KZuuPu8UjnbmFC9q9P65rVub',2),(36,'v587qzcywFhpBEXZG2ERLdsn7aT8R2wE',2),(37,'EVrFjMshunwLLWgrtb5g9RXK4GdLBLQe',2),(38,'GwKUCuexLKwTF7PtQZgSg4F6qMTBXCpC',2);
/*!40000 ALTER TABLE `trongate_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-02  5:40:57
