-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: dpt_weex_live
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aldi_exceptions`
--

DROP TABLE IF EXISTS `aldi_exceptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `aldi_exceptions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `log_id` int NOT NULL,
  `error_message` text NOT NULL,
  `order_id_pk` int DEFAULT NULL,
  `type` int NOT NULL DEFAULT '1' COMMENT '1: Exception 2: Message 3: HQ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aldi_exceptions`
--

LOCK TABLES `aldi_exceptions` WRITE;
/*!40000 ALTER TABLE `aldi_exceptions` DISABLE KEYS */;
INSERT INTO `aldi_exceptions` VALUES (1,340980,'{\"function\":\"insertReturn\",\"error\":{\"message\":\"ER_DUP_ENTRY: Duplicate entry \'B01072298P67\' for key \'return_master.order_id\'\",\"sql\":\"INSERT INTO `return_master`(`id`, `order_id`, `email`, `phone`, `address_type`, `salutation`, `first_name`, `last_name`, `street_name`, `street_number`, `remarks`, `postal_code`, `town`, `pack_station_number`, `post_number`, `post_office_number`, `country_code`, `company_name`, `created_at`, `source`) VALUES (DEFAULT, \'B01072298P67\', \'rlitau23@gmail.com\', NULL, \'DEFAULT\', \'MR\', \'Roman\', \'Litau\', \'Weidestraße\', \'31\', \'\', \'22083\', \'Hamburg\', \'\', \'\', \'\', \'DE\', \'aldi_north\', \'2021-11-30 08:40:09\', \'ALDI_AEC\')\"},\"log_id\":340980}',NULL,1),(2,341687,'{\"function\":\"insertReturn\",\"error\":{\"message\":\"ER_DUP_ENTRY: Duplicate entry \'B01075792P82\' for key \'return_master.order_id\'\",\"sql\":\"INSERT INTO `return_master`(`id`, `order_id`, `email`, `phone`, `address_type`, `salutation`, `first_name`, `last_name`, `street_name`, `street_number`, `remarks`, `postal_code`, `town`, `pack_station_number`, `post_number`, `post_office_number`, `country_code`, `company_name`, `created_at`, `source`) VALUES (DEFAULT, \'B01075792P82\', \'oliver.schnau@me.com\', NULL, \'DEFAULT\', \'MR\', \'Oliver\', \'Schnau\', \'Buchenweg\', \'8\', \'\', \'27251\', \'Neuenkirchen\', \'\', \'\', \'\', \'DE\', \'aldi_north\', \'2021-11-30 10:50:11\', \'ALDI_AEC\')\"},\"log_id\":341687}',NULL,1),(3,353284,'{\"function\":\"insertReturn\",\"error\":{\"message\":\"ER_DUP_ENTRY: Duplicate entry \'B01071795P17\' for key \'return_master.order_id\'\",\"sql\":\"INSERT INTO `return_master`(`id`, `order_id`, `email`, `phone`, `address_type`, `salutation`, `first_name`, `last_name`, `street_name`, `street_number`, `remarks`, `postal_code`, `town`, `pack_station_number`, `post_number`, `post_office_number`, `country_code`, `company_name`, `created_at`, `source`) VALUES (DEFAULT, \'B01071795P17\', \'willygruslak@web.de\', NULL, \'DEFAULT\', \'MR\', \'Willy\', \'Gruslak\', \'Erwin-Bahnmüller-Str.\', \'15\', \'\', \'71394\', \'Kernen im Remstal\', \'\', \'\', \'\', \'DE\', \'aldi_south\', \'2021-12-01 06:10:12\', \'ALDI_AEC\')\"},\"log_id\":353284}',NULL,1),(4,359558,'{\"function\":\"insertReturn\",\"error\":{\"message\":\"ER_DUP_ENTRY: Duplicate entry \'B01073493P48\' for key \'return_master.order_id\'\",\"sql\":\"INSERT INTO `return_master`(`id`, `order_id`, `email`, `phone`, `address_type`, `salutation`, `first_name`, `last_name`, `street_name`, `street_number`, `remarks`, `postal_code`, `town`, `pack_station_number`, `post_number`, `post_office_number`, `country_code`, `company_name`, `created_at`, `source`) VALUES (DEFAULT, \'B01073493P48\', \'salmon.k@web.de\', NULL, \'DEFAULT\', \'MRS\', \'Andrea\', \'Heinrichs\', \'Akazienstraße\', \'86\', \'\', \'41239\', \'Mönchengladbach\', \'\', \'\', \'\', \'DE\', \'aldi_south\', \'2021-12-01 18:40:13\', \'ALDI_AEC\')\"},\"log_id\":359558}',NULL,1);
/*!40000 ALTER TABLE `aldi_exceptions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-02  5:40:36
