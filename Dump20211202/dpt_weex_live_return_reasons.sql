-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: dpt_weex_live
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `return_reasons`
--

DROP TABLE IF EXISTS `return_reasons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `return_reasons` (
  `id` int NOT NULL AUTO_INCREMENT,
  `return_reason_value` varchar(100) NOT NULL,
  `return_reason` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `return_reasons`
--

LOCK TABLES `return_reasons` WRITE;
/*!40000 ALTER TABLE `return_reasons` DISABLE KEYS */;
INSERT INTO `return_reasons` VALUES (1,'DAMAGEDINTRANSIT','Damaged Transist'),(2,'LATEDELIVERY','Late Delivery'),(3,'PRICEMATCH','Price Match'),(4,'LOSTINTRANSIT','Lost in Transist'),(5,'MANUFACTURINGFAULT','Manfacturing fault'),(6,'WRONGDESCRIPTION','Wrong Description'),(7,'MISSEDLINKDEAL','Missed Linkdeal'),(8,'MISPICKWRONGITEMDELIVERED','Mispick wrong item delivered'),(9,'MISPICKITEMMISSING','Mispick Item missing'),(10,'SITEERROR','Site Error'),(11,'GOODWILL','Good will'),(12,'FOUNDCHEAPERPRICE','Found cheaper price'),(13,'DONTLIKEANYMORE','Dont like anymore'),(14,'ITEMBROKENORDAMAGED','Item broken or damaged'),(15,'SHIPPINGPACKAGINGDAMAGED','Shipping packaging damaged'),(16,'MISSINGPARTSORACCESSORIESMISSING','Missing parts or accessories'),(17,'ITEMTOOBIGORSMALL','Item too big or small'),(18,'NONE','None');
/*!40000 ALTER TABLE `return_reasons` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-02  5:41:34
