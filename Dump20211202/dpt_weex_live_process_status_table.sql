-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: dpt_weex_live
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `process_status_table`
--

DROP TABLE IF EXISTS `process_status_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `process_status_table` (
  `id` int NOT NULL AUTO_INCREMENT,
  `src_prid` varchar(50) NOT NULL,
  `target` varchar(50) NOT NULL,
  `process_type` varchar(50) NOT NULL,
  `process_status` varchar(50) NOT NULL,
  `first_updated` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `process_status_table`
--

LOCK TABLES `process_status_table` WRITE;
/*!40000 ALTER TABLE `process_status_table` DISABLE KEYS */;
INSERT INTO `process_status_table` VALUES (1,'PRID_10_1_20_20','PRID_10_1_40_20','new_order_check','ready','2021-09-27 09:27:14','2021-09-27 09:26:37'),(2,'PRID_10_1_20_40','PRID_10_1_40_20','save_order_response','ready','2020-07-01 11:54:22','0000-00-00 00:00:00'),(3,'PRID_10_1_20_100','PRID_10_1_60_20','whv_generation','done','2021-12-02 05:37:13','2021-12-02 05:41:25'),(4,'PRID_10_1_20_120','PRID_10_1_60_20','whv_response','ready','2021-12-02 05:41:13','2021-12-02 05:41:00'),(5,'PRID_10_1_20_140','PRID_10_1_60_20','generate_delivery_note','ready','2020-07-03 11:34:33','2020-07-03 11:33:45'),(6,'PRID_10_1_20_160','PRID_10_1_60_20','generate_label','ready','2020-07-03 11:37:33','2020-07-03 11:37:04'),(7,'PRID_10_1_20_220','PRID_10_1_80_20','carrier_registration_delivery','ready','2020-07-03 11:35:33','2020-07-03 11:35:19'),(8,'PRID_10_1_20_240','PRID_10_1_80_20','carrier_registration_delivery_response','ready','2020-07-03 11:36:33','2020-07-03 11:35:34'),(9,'PRID_10_1_20_180','PRID_10_1_40_20','generate_update_delivery','ready','2020-07-03 11:34:33','2020-07-03 11:34:24'),(10,'PRID_10_1_20_200','PRID_10_1_40_20','delivery_confirmation_aldi','ready','2021-12-02 05:15:13','2021-12-02 05:30:03'),(11,'PRID_10_1_20_260','PRID_10_1_40_20','trackingid_to_aldi','ready','2020-07-03 11:35:33','2020-07-03 11:35:00'),(12,'PRID_10_1_20_280','PRID_10_1_40_20','trackingid_confirmation_aldi','ready','2020-07-03 11:35:33','2020-07-03 11:34:39'),(13,'PRID_10_1_20_300','PRID_10_1_80_20','request_delivery_update_to_carrier','ready','2020-07-01 11:54:22','0000-00-00 00:00:00'),(14,'PRID_10_1_20_320','PRID_10_1_80_20','response_delivery_update_from_carrier','ready','2020-06-26 11:54:56','0000-00-00 00:00:00'),(15,'PRID_10_1_20_340','PRID_10_1_40_20','update_delivery_as_delivered_to_aldi','ready','2020-07-01 11:54:22','0000-00-00 00:00:00'),(16,'PRID_10_1_20_360','PRID_10_1_40_20','update_delivery_response_from_aldi','ready','2020-06-26 11:54:45','0000-00-00 00:00:00'),(17,'PRID_10_1_20_380','PRID_10_1_40_20','fetch_returns','ready','2020-07-01 11:54:22','2021-12-01 14:41:30'),(18,'PRID_10_1_20_400','PRID_10_1_40_20','return_confirmation_from_aldi','ready','2020-06-26 12:23:13','0000-00-00 00:00:00'),(19,'PRID_10_1_20_60','PRID_10_1_40_20','reject_order_info_aldi','ready','2020-06-26 12:34:28','0000-00-00 00:00:00'),(20,'PRID_10_1_20_80','PRID_10_1_40_20','reject_order_confirmation_aldi','ready','2020-06-26 12:34:28','0000-00-00 00:00:00'),(21,'PRID_10_1_20_100','PRID_10_1_60_20','whv_files_upload_ftp','ready','2020-07-03 11:36:33','2020-11-09 14:21:16'),(22,'PRID_10_1_20_20 ','PRID_10_1_40_20','update_order_status','ready','2021-12-02 05:39:13','2021-12-02 05:38:01'),(23,'PRID_TRACK_RETURN_STATUS','PRID_TRACK_RETURN_STATUS','track_return_status','ready','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `process_status_table` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-02  5:41:33
