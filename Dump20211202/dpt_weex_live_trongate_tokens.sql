-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: dpt_weex_live
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `trongate_tokens`
--

DROP TABLE IF EXISTS `trongate_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `trongate_tokens` (
  `id` int NOT NULL AUTO_INCREMENT,
  `token` varchar(125) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `user_id` int DEFAULT '0',
  `expiry_date` int DEFAULT NULL,
  `code` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6944 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trongate_tokens`
--

LOCK TABLES `trongate_tokens` WRITE;
/*!40000 ALTER TABLE `trongate_tokens` DISABLE KEYS */;
INSERT INTO `trongate_tokens` VALUES (6909,'g7dODOAKMc_KEjM4q9MyJDNszHf4Wei5',7,1638401908,'0'),(6912,'xm6Lapt67Zzgpa85fUlZplvgoRpKIsOT',17,1638423591,'0'),(6913,'WRLSVXpA0J4J4QJ_GWf0mYwxY47KpEgt',11,1638424535,'0'),(6914,'AaXjtBtn4BxhzF9PXe5RapxXXynmwPRO',19,1638425939,'0'),(6915,'k9p9IXf8Zumh5ObRZLhvTPksUqP_RuhY',17,1638426916,'0'),(6916,'XIfSKaR90G1wkcDA_M__orwhRK4oL9sg',30,1638426974,'0'),(6917,'X57Xf6HL2RqRbxqVPmYXRLLKYTO6P-DW',31,1638427793,'0'),(6918,'O7DFfOiIiufSmUNA3hQRii9At939Nz_c',11,1638428198,'0'),(6919,'7A_T7ZkayD4vLruVssLBpiT7fX2F7--7',24,1638428944,'0'),(6920,'gPGYkwbPQExpu7Zsd2-PV8SHUN-2zhHC',24,1638428957,'0'),(6921,'C_0k5zrDTRgL-j2R2OH6W2b0-3VmbKuB',11,1638429311,'0'),(6922,'JbhPvKqsISw2S0BTptCPg8vRBp6eT2k8',12,1638430519,'0'),(6923,'S6LfGG6bFx_8WjJLOVkB4pnYkrxAo05s',9,1638430751,'0'),(6924,'9g7ai4vsx0dI4XDQoJgGSMe7JkEXXMSM',29,1638431199,'0'),(6925,'bywUFYm7o8Xy1hDQB16KWzMO-UjL11uO',22,1638431596,'0'),(6926,'G9ZuiQzxloJ7amdgQ-k9KLy4d_BZJKK6',7,1638440523,'0'),(6927,'uAfP79cPqA7ZoPaHo9dpHKZ_0leMEgc7',22,1638443969,'0'),(6928,'bU55Rxtl2AFtVxwZNqh0Jk2DHBAQtW6O',19,1638444302,'0'),(6929,'jZgLNK9C-YmagW97R6QLkxDzFogPLr_u',17,1638449068,'0'),(6930,'b3wfw4Btb13-qh0zPWhiqIupTs1MixfN',29,1638449752,'0'),(6931,'vfauBJ3YjaAyDaM1ZQiq-UZcI2H-yxw9',12,1638449869,'0'),(6932,'fy1385u-BcfdyQTUk7NsSIxGyZebz8sS',12,1638450654,'0'),(6933,'6VnGlPswk5HBCRJTlv9SOv3eYhG_4jM9',31,1638450983,'0'),(6934,'gkVv_4Obs6EQJ_sA-kE7iwqMAiYNmgke',7,1638451472,'0'),(6935,'_SczCjhZQNlttmk4P2KTsMxtvnUSOF3x',33,1638453390,'0'),(6936,'LK6BiGEyPqKclPlz3uPhRKU5A2v528sF',24,1638454152,'0'),(6937,'8Y7wFCB0Zsl_8XqH8yEuc4miNTu9i7eZ',7,1638454363,'0'),(6938,'1vChojsWbQk1YKefOcHWlmypyyD-yqMg',31,1638455269,'0'),(6939,'d5Y6xf_E6ym5IBMSnh7Ti-VLwCqMhBTg',26,1638457239,'0'),(6940,'haMPDNP3bygxTSGul3MOR54OJ5Y7DVwH',1,1638462495,'0'),(6941,'ZA_i18zBC4YVWxLG3DlPttl8ScC83-tI',4,1638463513,'0'),(6942,'OCqaRJ4-QxMPb3XvBGfMjjAosopXyOGf',7,1638476648,'0'),(6943,'p2JzgkSEj_Weq5aQSw_H7fmxJfRj0MY5',17,1638504821,'0');
/*!40000 ALTER TABLE `trongate_tokens` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-02  5:41:13
