-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: dpt_weex_live
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dt_form_settings`
--

DROP TABLE IF EXISTS `dt_form_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dt_form_settings` (
  `id` int NOT NULL,
  `field_name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `is_mandatory` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `alert_message` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `company_id` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `created_on` datetime NOT NULL,
  `created_by` int NOT NULL,
  `form_name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dt_form_settings`
--

LOCK TABLES `dt_form_settings` WRITE;
/*!40000 ALTER TABLE `dt_form_settings` DISABLE KEYS */;
INSERT INTO `dt_form_settings` VALUES (19,'company_name','yes','Company Name is mandatory','','2020-06-02 15:38:25',1,'company_creation_form'),(20,'company_logo','no','Company Logo is mandatory','','2020-06-02 15:38:25',1,'company_creation_form'),(21,'company_type','yes','Company type is mandatory','','2020-06-02 15:38:25',1,'company_creation_form'),(22,'telephone','yes','Company telefone is mandatory','','2020-06-02 15:38:25',1,'company_creation_form'),(23,'email_id','yes','Company email is mandatory','','2020-06-02 15:38:25',1,'company_creation_form'),(24,'website_name','yes','','','2020-06-02 15:38:25',1,'company_creation_form'),(25,'kontact_details','yes','Company contact is mandatory','','2020-06-02 15:38:25',1,'company_creation_form'),(26,'company_address','yes','Company address is mandatory','','2020-06-02 15:38:25',1,'company_creation_form');
/*!40000 ALTER TABLE `dt_form_settings` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-02  5:40:47
