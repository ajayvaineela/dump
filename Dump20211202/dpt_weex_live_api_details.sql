-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: dpt_weex_live
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `api_details`
--

DROP TABLE IF EXISTS `api_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `api_details` (
  `id` int NOT NULL AUTO_INCREMENT,
  `company_id` bigint NOT NULL,
  `requested_url` text NOT NULL,
  `query_string` varchar(10) NOT NULL,
  `requested_method` varchar(10) NOT NULL,
  `requested_service` varchar(255) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `client_id` varchar(50) NOT NULL,
  `client_secret` varchar(50) NOT NULL,
  `grant_type` varchar(50) NOT NULL,
  `auth_token` varchar(255) NOT NULL,
  `company_name` varchar(50) NOT NULL,
  `status` tinyint NOT NULL,
  `channel_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `api_details`
--

LOCK TABLES `api_details` WRITE;
/*!40000 ALTER TABLE `api_details` DISABLE KEYS */;
INSERT INTO `api_details` VALUES (1,1000333,'https://aldivendorwebservices.co6hnkccbu-aldiitpla1-p1-public.model-t.cc.commerce.ondemand.com/authorizationserver/oauth/token','','POST','token_generation','monolith_DE','VnopHbIGlWw5PIs18C8G','monolith_DE','zqcjw8b797zSBRmuvG8v','password','Authorization: Bearer','aldi_north',1,''),(2,1000444,'https://portal.aldi-liefert.de/admin/supplier/orders?page=1&size=100','','GET','fetch_open_orders','','','','','','homeDeliveryToken: 3fd8b244363b4d2d4a9219c28f6d08b9','aldi_south',1,''),(3,1000333,'https://aldivendorwebservices.co6hnkccbu-aldiitpla1-p1-public.model-t.cc.commerce.ondemand.com/aldivendorwebservices/1.0/DE/vendor/monolith_DE/orders?pageSize=100','','GET','fetch_open_orders','','','','','','Authorization: Bearer','aldi_north',1,''),(4,1000333,'https://aldivendorwebservices.co6hnkccbu-aldiitpla1-p1-public.model-t.cc.commerce.ondemand.com/aldivendorwebservices/1.0/DE/vendor/monolith_DE/orders/','yes','PUT','confirm_orders','','','','','','Authorization: Bearer','aldi_north',1,''),(5,1000333,'https://aldivendorwebservices.co6hnkccbu-aldiitpla1-p1-public.model-t.cc.commerce.ondemand.com/aldivendorwebservices/1.0/DE/vendor/monolith_DE/orders/','yes','POST','generate_delivery','','','','','','Authorization: Bearer','aldi_north',1,''),(6,1000444,'https://portal.aldi-liefert.de/admin/supplier/orders/confirm','','POST','confirm_orders','','','','','','homeDeliveryToken: 3fd8b244363b4d2d4a9219c28f6d08b9','aldi_south',1,''),(7,1000444,'https://portal.aldi-liefert.de/admin/supplier/orders/delivered','','POST','generate_delivery','','','','','','homeDeliveryToken: 3fd8b244363b4d2d4a9219c28f6d08b9','aldi_south',1,''),(8,1000444,'https://portal.aldi-liefert.de/admin/supplier/orders/returned','','POST','generate_returns','','','','','','homeDeliveryToken: 3fd8b244363b4d2d4a9219c28f6d08b9','aldi_south',1,''),(9,1000333,'https://aldivendorwebservices.co6hnkccbu-aldiitpla1-p1-public.model-t.cc.commerce.ondemand.com/aldivendorwebservices/1.0/DE/vendor/monolith_DE/returnrequests?pageSize=100','','GET','fetch_returns','','','','','','Authorization: Bearer','aldi_north',1,''),(10,1000333,'https://aldivendorwebservices.co6hnkccbu-aldiitpla1-p1-public.model-t.cc.commerce.ondemand.com/aldivendorwebservices/1.0/DE/vendor/monolith_DE/orders/','yes','POST','generate_returns','','','','','','Authorization: Bearer','aldi_north',1,''),(11,1000333,'https://aldivendorwebservices.co6hnkccbu-aldiitpla1-p1-public.model-t.cc.commerce.ondemand.com/aldivendorwebservices/1.0/DE/vendor/monolith_DE/orders/','yes','PUT','update_returns','','','','','','Authorization: Bearer','aldi_north',1,''),(12,1000333,'https://aldivendorwebservices.co6hnkccbu-aldiitpla1-p1-public.model-t.cc.commerce.ondemand.com/aldivendorwebservices/1.0/DE/vendor/monolith_DE/orders/','yes','GET','fetch_return_code','','','','','','Authorization: Bearer','aldi_north',1,''),(13,1000333,'https://aldivendorwebservices.co6hnkccbu-aldiitpla1-p1-public.model-t.cc.commerce.ondemand.com/aldivendorwebservices/2.0/DE/vendor/monolith_DE/orders/','yes','POST','cancellations','','','','','','Authorization: Bearer','aldi_north',1,''),(14,1000333,'https://aldivendorwebservices.co6hnkccbu-aldiitpla1-p1-public.model-t.cc.commerce.ondemand.com/aldivendorwebservices/1.0/DE/vendor/monolith_DE/orders/','yes','GET','order_status_check','','','','','','Authorization: Bearer','aldi_north',1,''),(15,1000444,'https://portal.aldi-liefert.de/admin/supplier/return','','GET','fetch_south_returns','','','','','','homeDeliveryToken: 3fd8b244363b4d2d4a9219c28f6d08b9','aldi_south',1,''),(16,1000444,'https://portal.aldi-liefert.de/admin/supplier/return/confirm','','POST','confirm_south_returns','','','','','','homeDeliveryToken: 3fd8b244363b4d2d4a9219c28f6d08b9','aldi_south',1,''),(17,1000444,'https://portal.aldi-liefert.de/admin/supplier/orders/info/','','GET','fetch_return_status','','','','','','homeDeliveryToken: 3fd8b244363b4d2d4a9219c28f6d08b9','aldi_south',1,''),(18,1000333,'https://aldivendorwebservices.co6hnkccbu-aldiitpla1-p1-public.model-t.cc.commerce.ondemand.com/aldivendorwebservices/1.0/DE/vendor/monolith_DE/orders','','GET','fetch_return_status','','','','','','Authorization: Bearer','aldi_north',1,''),(19,1000333,'https://aldivendorwebservices.co6hnkccbu-aldiitpla1-p1-public.model-t.cc.commerce.ondemand.com/aldivendorwebservices/1.0/DE/vendor/monolith_DE/orders','','GET','return_status_check','','','','','','Authorization: Bearer','aldi_north',1,''),(20,0,'https://aldivendorwebservices.co6hnkccbu-aldiitpla2-p1-public.model-t.cc.commerce.ondemand.com/aldivendorwebservices/2.0/DE/vendor/monolith_DE/orders/','','POST','cancellations','','','','','','Authorization: Bearer','aldi_north',1,'ALDI_AEC'),(21,1000333,'https://aldivendorwebservices.co6hnkccbu-aldiitpla2-p1-public.model-t.cc.commerce.ondemand.com/authorizationserver/oauth/token','','POST','token_generation_v2','monolith_DE','ggDS2KgbM4W8NPxCuW4R','monolith_DE','vz4l2sqFG034ieZ4YNbt','password','Authorization: Bearer','aldi_north',1,'ALDI_AEC');
/*!40000 ALTER TABLE `api_details` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-02  5:40:17
